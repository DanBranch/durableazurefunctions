using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Extensions.Logging;
using ServelessOrchestration.Requests;

namespace ServelessOrchestration
{
    public static class Starter
    {
        [FunctionName("HttpStart")]
        public static async Task<HttpResponseMessage> Run(
            [HttpTrigger(AuthorizationLevel.Function, "POST")]HttpRequestMessage req,
            [OrchestrationClient]DurableOrchestrationClientBase starter,
            ILogger log)
        {
            var orderData = await req.Content.ReadAsAsync<OrderRequestRequest>();

            // Function input comes from the request content.
            var instanceId  = await starter.StartNewAsync("CheckOut", orderData);

            log.LogInformation($"Started orchestration with ID = '{instanceId }'.");

            return starter.CreateCheckStatusResponse(req, instanceId);
        }
    }
}
