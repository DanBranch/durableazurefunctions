﻿using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using Microsoft.Extensions.Logging;
using ServelessOrchestration.Requests;

namespace ServelessOrchestration
{
    public static class Orchestrator
    {
        [FunctionName("CheckOut")]
        public static async Task<string> Run([OrchestrationTrigger] DurableOrchestrationContextBase context, ILogger log)
        {
            var orderData = context.GetInput<OrderRequestRequest>();
            
            await context.CallActivityAsync<bool>("CheckAndReserveInventory", orderData);
            await context.CallActivityAsync<bool>("ProcessPayment", orderData);
            var trackingNumber = await context.CallActivityAsync<string>("ScheduleShippingWithThirdParty", orderData);
            await context.CallActivityAsync<bool>("ConfirmationEmail", trackingNumber);

            return trackingNumber;
        }
    }
}
