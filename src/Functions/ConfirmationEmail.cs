using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using Microsoft.Extensions.Logging;

namespace ServelessOrchestration.Functions
{
    public static class ConfirmationEmail
    {
        [FunctionName("ConfirmationEmail")]
        public static async Task<bool> Run([ActivityTrigger] string trackingNumber, ILogger log)
        {
            log.LogInformation($"Sending email. track number: {trackingNumber}");
            await Task.Delay(1000);
            return true;
        }
    }
}