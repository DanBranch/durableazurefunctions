using System;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using Microsoft.Extensions.Logging;
using ServelessOrchestration.Requests;

namespace ServelessOrchestration.Functions
{
    public static class ScheduleShippingWithThirdParty
    {
        [FunctionName("ScheduleShippingWithThirdParty")]
        public static async Task<string> Run([ActivityTrigger] DurableActivityContext context, ILogger log)
        {
            var orderData = context.GetInput<OrderRequestRequest>();
            log.LogInformation("ScheduleShippingWithThirdParty");
            await Task.Delay(4000);

            return Guid.NewGuid().ToString();
        }
    }
}