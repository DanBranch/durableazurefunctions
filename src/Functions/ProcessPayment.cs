using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using Microsoft.Extensions.Logging;
using ServelessOrchestration.Requests;

namespace ServelessOrchestration.Functions
{
    public static class ProcessPayment
    {
        [FunctionName("ProcessPayment")]
        public static async Task<bool> Run([ActivityTrigger] DurableActivityContext context, ILogger log)
        {
            var orderData = context.GetInput<OrderRequestRequest>();
            log.LogInformation("Payment stuff.");
            await Task.Delay(3000);

            return true;
        }
    }
}