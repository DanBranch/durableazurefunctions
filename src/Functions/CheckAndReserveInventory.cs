using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using Microsoft.Extensions.Logging;
using ServelessOrchestration.Requests;

namespace ServelessOrchestration.Functions
{
    public static class CheckAndReserveInventory
    {
        [FunctionName("CheckAndReserveInventory")]
        public static async Task<bool> Run([ActivityTrigger] DurableActivityContext context, ILogger log)
        {
            var orderData = context.GetInput<OrderRequestRequest>();
            log.LogInformation("Doing inventory checks.");
            await Task.Delay(2000);
            return true;
        }
    }
}