using System;

namespace ServelessOrchestration.Requests
{
    public class Item
    {
        public Guid Id { get; set; }

        public decimal UnitPrice { get; set; }

        public int Quantity { get; set; }
    }
}