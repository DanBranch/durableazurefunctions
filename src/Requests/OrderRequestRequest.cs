using System;
using System.Collections.Generic;

namespace ServelessOrchestration.Requests
{
    public class OrderRequestRequest
    {
        public Guid UserId { get; set; }

        public List<Item> Items { get; set; }
    }
}