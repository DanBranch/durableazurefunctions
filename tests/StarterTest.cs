using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using Microsoft.Extensions.Logging;
using Moq;
using Xunit;

namespace ServelessOrchestration.Tests
{
    public class StarterTest
    {
        [Fact]
        public async Task HttpStart_returns_OK()
        {
            // ARRANGE
            const string functionName = "CheckOut";
            const string instanceId   = "7E467BDB-213F-407A-B86A-1954053D3C24";

            var loggerMock = new Mock<ILogger>();
            var durableOrchestrationClientBaseMock = new Mock<DurableOrchestrationClientBase>();

            durableOrchestrationClientBaseMock
                .Setup(x => x.StartNewAsync(functionName, It.IsAny<object>()))
                .ReturnsAsync(instanceId);

            durableOrchestrationClientBaseMock
                .Setup(x => x.CreateCheckStatusResponse(It.IsAny<HttpRequestMessage>(), instanceId))
                .Returns(new HttpResponseMessage
                {
                    StatusCode = HttpStatusCode.OK,
                    Content    = new StringContent(string.Empty),
                });

            // ACT
            var response = await Starter.Run(
                new HttpRequestMessage
                {
                    Content    = new StringContent("{}", Encoding.UTF8, "application/json"),
                },
                durableOrchestrationClientBaseMock.Object,
                loggerMock.Object);

            // ASSERT
            Assert.True(response.StatusCode == HttpStatusCode.OK);
        }
    }
}
