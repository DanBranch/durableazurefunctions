using System;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using Microsoft.Extensions.Logging;
using Moq;
using ServelessOrchestration.Requests;
using Xunit;

namespace ServelessOrchestration.Tests
{
    public class OrchestratorTest
    {
        [Fact]
        public async Task Run_returns_trackingNumber()
        {
            // ARRANGE
            var trackingNumber = Guid.NewGuid().ToString();

            var durableOrchestrationContextMock = new Mock<DurableOrchestrationContextBase>();
            var loggerMock = new Mock<ILogger>();
            
            durableOrchestrationContextMock
                .Setup(x => x.CallActivityAsync<bool>("CheckAndReserveInventory", It.IsAny<OrderRequestRequest>()))
                .ReturnsAsync(true);

            durableOrchestrationContextMock
                .Setup(x => x.CallActivityAsync<bool>("ProcessPayment", It.IsAny<OrderRequestRequest>()))
                .ReturnsAsync(true);

            durableOrchestrationContextMock
                .Setup(x => x.CallActivityAsync<string>("ScheduleShippingWithThirdParty", It.IsAny<OrderRequestRequest>()))
                .ReturnsAsync(trackingNumber);

            durableOrchestrationContextMock
                .Setup(x => x.CallActivityAsync<bool>("ConfirmationEmail", trackingNumber))
                .ReturnsAsync(true);

            // ACT
            var result = await Orchestrator.Run(durableOrchestrationContextMock.Object, loggerMock.Object);

            // ASSERT
            Assert.Equal(trackingNumber, result);
        }
    }
}
